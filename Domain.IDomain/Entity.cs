﻿using PX.Code.Base;
using PX.Code.Core.EntityDAO;
using PX.Core;
using PX.Core.List;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Domain.IDomain
{
    public class Entity<TEntity> : EntityBase where TEntity : EntityBase, new()
    {
        /// <summary>
        /// 插入数据
        /// </summary>
        /// <returns></returns>
        public override Int32 Insert()
        {
            return EntityDAO<TEntity>._.Insert(this as TEntity);
        }

        /// <summary>更新数据库</summary>
        /// <returns></returns>
        public override Boolean Update()
        {
            return EntityDAO<TEntity>._.Update(this as TEntity);
        }

        /// <summary>从数据库中删除该对象</summary>
        /// <returns></returns>
        public override Boolean Delete()
        {
            return EntityDAO<TEntity>._.Delete(this as TEntity);
        }

        /// <summary></summary>
        /// <returns></returns>
        public override Boolean Save()
        {
            return EntityDAO<TEntity>._.Save(this as TEntity);
        }

        #region 获取/设置 字段值
        /// <summary></summary>
        /// <param name="name">字段名</param>
        /// <returns></returns>
        public override Object this[String name] 
        {
            get { return null; }
            set { throw new Exception("没有包含此属性。"); }
        }
        /// <summary>设置字段值，该方法影响脏数据。</summary>
        /// <param name="name">字段名</param>
        /// <param name="value">值</param>
        /// <returns>返回是否成功设置了数据</returns>
        public override Boolean SetItem(String name, Object value)
        {
            this[name] = value;
            return true;
        }
        #endregion
        public static Field FindFieldByName(string name)
        {
            return EntityDAO<TEntity>.entitySession.TableItem.FindByName(name);
        }

        /// <summary>
        /// 操作方法的静态扩展,多用于类内部属性的扩展,对外封闭
        /// </summary>
        protected class _base
        {
            /// <summary>从数据库中删除该对象</summary>
            /// <returns></returns>
            public static Int32 DeleteAll(AbstractExpressions icriterion)
            {
                return EntityDAO<TEntity>._.DeleteAll(icriterion);
            }
            public static ListRich<TEntity> FindAll()
            {
                return ListRich<TEntity>.From(EntityDAO<TEntity>._.FindAll());
            }
            public static ListRich<TEntity> FindAll(String strWhere)
            {
                var lsit = EntityDAO<TEntity>._.FindAll(strWhere, string.Empty);
                return ListRich<TEntity>.From(EntityDAO<TEntity>._.FindAll(strWhere, String.Empty));
            }
            public static ListRich<TEntity> FindAll(String field, Object obj)
            {
                return ListRich<TEntity>.From(EntityDAO<TEntity>._.FindAll(field, obj));
            }
            public static ListRich<TEntity> FindAll(String[] fields, Object[] obj)
            {
                return ListRich<TEntity>.From(EntityDAO<TEntity>._.FindAll(fields, obj));
            }
            public static ListRich<TEntity> FindAll(AbstractExpressions icriterion, PxOrder order)
            {
                return ListRich<TEntity>.From(EntityDAO<TEntity>._.FindAll(icriterion, order));
            }
            public static ListRich<TEntity> FindAll(AbstractExpressions icriterion)
            {
                return ListRich<TEntity>.From(EntityDAO<TEntity>._.FindAll(icriterion));
            }
            public static ListRich<TEntity> FindAll(AbstractExpressions icriterion, PxOrder order, Int32 startRowIndex, Int32 maximumRows, out Int32 rowCount)
            {
                return ListRich<TEntity>.From(EntityDAO<TEntity>._.FindAll(icriterion, order, startRowIndex, maximumRows, out rowCount));
            }
            public static ListRich<TEntity> FindAll(String icriterion, PxOrder order, Int32 startRowIndex, Int32 maximumRows, out Int32 rowCount)
            {
                return ListRich<TEntity>.From(EntityDAO<TEntity>._.FindAll(icriterion, order, startRowIndex, maximumRows, out rowCount));
            }
            public static TEntity Find(String field, Object obj)
            {
                return EntityDAO<TEntity>._.Find(field, obj);
            }
            public static TEntity Find(String[] fields, Object[] obj)
            {
                return EntityDAO<TEntity>._.Find(fields, obj);
            }
            public static TEntity Find(AbstractExpressions icriterion)
            {
                return EntityDAO<TEntity>._.Find(icriterion);
            }
            public static TEntity FindByID(Object id)
            {
                return EntityDAO<TEntity>._.FindByID(id);
            }
            public static TEntity LoadData(DataRow dr)
            {
                return EntityDAO<TEntity>._.LoadData(dr);
            }
        }
    }
}
