﻿using PX.Code.Base;
using PX.Code.Base.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.IDomain.IDomian
{
    /// <summary>BS_EMPLOYEE接口</summary>
    /// <remarks></remarks>
    public partial interface IBS_EMPLOYEE:IEntity
    {
        #region 属性
         /// <summary></summary>
         Int32 ID { get; set; }
         /// <summary></summary>
         String EMP_NAME { get; set; }
         /// <summary></summary>
         Decimal ENTERPRISE_ID { get; set; }
         /// <summary></summary>
         Decimal DEPART_ID { get; set; }
         /// <summary></summary>
         DateTime BIRTHDAY { get; set; }
         /// <summary></summary>
         String ID_CARD { get; set; }
         /// <summary></summary>
         DateTime HIRE_DATE { get; set; }
         /// <summary></summary>
         String MOBILE { get; set; }
         /// <summary></summary>
         String PHONE { get; set; }
         /// <summary></summary>
         DateTime LEAVE_DATE { get; set; }
         /// <summary></summary>
         Decimal IS_ENABLE { get; set; }
         /// <summary></summary>
         Decimal SEX { get; set; }
         /// <summary></summary>
         String ADDRESS { get; set; }
         /// <summary></summary>
         String DUTY { get; set; }
         /// <summary></summary>
         String USER_NAME { get; set; }
         /// <summary></summary>
         String PSW { get; set; }
        #endregion
        IBS_EMPLOYEE_ _X { get; }
     }
      public interface IBS_EMPLOYEE_
        {
            /// <summary></summary>
            Field ID { get;}
            /// <summary></summary>
            Field EMP_NAME { get;}
            /// <summary></summary>
            Field ENTERPRISE_ID { get;}
            /// <summary></summary>
            Field DEPART_ID { get;}
            /// <summary></summary>
            Field BIRTHDAY { get;}
            /// <summary></summary>
            Field ID_CARD { get;}
            /// <summary></summary>
            Field HIRE_DATE { get;}
            /// <summary></summary>
            Field MOBILE { get;}
            /// <summary></summary>
            Field PHONE { get;}
            /// <summary></summary>
            Field LEAVE_DATE { get;}
            /// <summary></summary>
            Field IS_ENABLE { get;}
            /// <summary></summary>
            Field SEX { get;}
            /// <summary></summary>
            Field ADDRESS { get;}
            /// <summary></summary>
            Field DUTY { get;}
            /// <summary></summary>
            Field USER_NAME { get;}
            /// <summary></summary>
            Field PSW { get;}
        }
}
