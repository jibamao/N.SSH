﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NewLife.Configuration;

namespace PX.Code.Hibernate
{
    internal class EntitySessionFactory
    {
        private static Dictionary<string, ISessionFactory> _EntitySessionFactory = null;
        private static Dictionary<string, NHibernate.Cfg.Configuration> _Configuration = null;

        public static Dictionary<string, NHibernate.Cfg.Configuration> getConfigurationDic
        {
            get { return EntitySessionFactory._Configuration; }
        }
        private static readonly object lockHelper = new object();
        public static Dictionary<string, ISessionFactory> getEntitySessionFactoryDic
        {
            get { return EntitySessionFactory._EntitySessionFactory; }
        }

        private EntitySessionFactory()
        {
            _EntitySessionFactory = new Dictionary<string, ISessionFactory>();
            _Configuration = new Dictionary<string, NHibernate.Cfg.Configuration>();
        }
        public static ISessionFactory getEntitySessionFactory(string strConName)
        {

            if (_EntitySessionFactory == null)
            {
                lock (lockHelper)
                {
                    if (_EntitySessionFactory == null || _Configuration==null)
                    {
                        new EntitySessionFactory();
                    }
                }
            }

            if (!_EntitySessionFactory.ContainsKey(strConName))
            {
                lock (lockHelper)
                {
                    if (!_EntitySessionFactory.ContainsKey(strConName))
                    {
                        string path = string.Empty;
                        if (System.Environment.CurrentDirectory == System.AppDomain.CurrentDomain.BaseDirectory)//Windows应用程序则相等
                        {
                            path = AppDomain.CurrentDomain.BaseDirectory;
                        }
                        else
                        {
                            path = AppDomain.CurrentDomain.BaseDirectory + "Bin";
                        }
                        string strPath = System.IO.Path.Combine(path, Config.AppSettings[strConName]);
                       
                        NHibernate.Cfg.Configuration configuration= new NHibernate.Cfg.Configuration().Configure(strPath);
                        _Configuration.Add(strConName, configuration);
                        _EntitySessionFactory.Add(strConName, configuration.BuildSessionFactory());
                    }

                }

            }
            return _EntitySessionFactory[strConName];
        }
        public static NHibernate.Cfg.Configuration getEntityConfiguration(string strConName)
        {
            if (!_Configuration.ContainsKey(strConName))
            {
                getEntitySessionFactory(strConName);
            }
            return _Configuration[strConName];
        }
    }
}
