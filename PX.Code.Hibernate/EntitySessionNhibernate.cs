﻿using NHibernate;
using NHibernate.Criterion;
using NHibernate.Mapping;
using PX.Code.Base;
using PX.Code.Base.Common;
using PX.Code.Base.Entity;
using PX.Code.Hibernate.Common;
using PX.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;


namespace PX.Code.Hibernate
{
    public class EntitySessionNhibernate<TEntity> : IEntitySession<TEntity> where TEntity:class,IEntity,new()
    {
        private ISessionFactory sessionFactory = null;
        private ITableItem tableItem = null;

        public ITableItem TableItem
        {
            get { return tableItem; }
        }

        private NHibernate.Cfg.Configuration configuration = null;

        public EntitySessionNhibernate(string strConfig)
        {
            //configuration = new NHibernate.Cfg.Configuration().Configure(System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, strConfig));
            sessionFactory = EntitySessionFactory.getEntitySessionFactory(strConfig);//configuration.BuildSessionFactory();
            configuration = EntitySessionFactory.getEntityConfiguration(strConfig);
            Type type = typeof(TEntity);
            PersistentClass prec = configuration.GetClassMapping(type.FullName);
            List<Column> list = prec.Table.ColumnIterator.ToList();
            List<Field> listResult = new List<Field>();
            foreach (var item in list)
            {
               listResult.Add(Field.CreateFieldItem(item.Name,item.Value.Type.ReturnedClass));
            }
            tableItem = PX.Code.Base.TableItem.CreateTableItem(listResult, type);
            //string sql = Restrictions.Eq("Name", "s").ToString();
            //ICriterion ic = Restrictions.Eq("Name", "s") & Restrictions.Eq("Name", "s");
            //string str= (Restrictions.Eq("Name", "s") & Restrictions.Eq("Name", "s")).ToString();

        }
        public EntitySessionNhibernate(Type type)
        {
            string strConfig=type.GetCustomAttribute<EntityAttribute>(true).ConnName;
            //configuration = new NHibernate.Cfg.Configuration().Configure(System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, strConfig));
            sessionFactory = EntitySessionFactory.getEntitySessionFactory(strConfig);//configuration.BuildSessionFactory();
            configuration = EntitySessionFactory.getEntityConfiguration(strConfig);
            PersistentClass prec = configuration.GetClassMapping(type.FullName);
            List<Column> list = prec.Table.ColumnIterator.ToList();
            List<Field> listResult = new List<Field>();
            foreach (var item in list)
            {
                listResult.Add(Field.CreateFieldItem(item.Name, item.Value.Type.ReturnedClass));
            }
            tableItem = PX.Code.Base.TableItem.CreateTableItem(listResult, type);
            //string sql = Restrictions.Eq("Name", "s").ToString();
            //ICriterion ic = Restrictions.Eq("Name", "s") & Restrictions.Eq("Name", "s");
            //string str= (Restrictions.Eq("Name", "s") & Restrictions.Eq("Name", "s")).ToString();

        }
        public EntitySessionNhibernate()
        {
            string strConfig =typeof(TEntity).GetCustomAttribute<EntityAttribute>(true).ConnName;
            //configuration = new NHibernate.Cfg.Configuration().Configure(System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, strConfig));
            sessionFactory = EntitySessionFactory.getEntitySessionFactory(strConfig);//configuration.BuildSessionFactory();
            configuration = EntitySessionFactory.getEntityConfiguration(strConfig);
            Type type = typeof(TEntity);
            PersistentClass prec = configuration.GetClassMapping(type.FullName);
            List<Column> list = prec.Table.ColumnIterator.ToList();
            List<Field> listResult = new List<Field>();
            foreach (var item in list)
            {
                listResult.Add(Field.CreateFieldItem(item.Name, item.Value.Type.ReturnedClass));
            }
            tableItem = PX.Code.Base.TableItem.CreateTableItem(listResult, type);
            //string sql = Restrictions.Eq("Name", "s").ToString();
            //ICriterion ic = Restrictions.Eq("Name", "s") & Restrictions.Eq("Name", "s");
            //string str= (Restrictions.Eq("Name", "s") & Restrictions.Eq("Name", "s")).ToString();

        }
        internal NHibernate.Cfg.Configuration Configuration
        {
            get { return configuration; }
            set { configuration = value; }
        }

        internal ISessionFactory getISessonFactory
        {
            get { return sessionFactory; }
        }
        internal ISession getNewISession
        {
            get
            { return sessionFactory.OpenSession(); }
        }
        private ISession _ISession = null;
        private EntitySessionModule mo = null;

        internal ISession getISession
        {
            get
            {
                if (_ISession == null)
                {
                    mo = new EntitySessionModule(sessionFactory);
                    mo.LessRequest();


                    _ISession = getISessonFactory.GetCurrentSession();
                }
                return _ISession;
            }
            set { _ISession = value; }
        }
        public Int32 Insert(IEntity entity)
        {
            Int32 result = -1;
            result = (Int32)getISession.Save(entity);
            getISession.Flush();
            return result;
        }

        /// <summary>更新数据库</summary>
        /// <returns></returns>
        public Boolean Update(IEntity entity)
        {

            getISession.Merge(entity);
            getISession.Flush();
            return true;

        }

        /// <summary>从数据库中删除该对象</summary>
        /// <returns></returns>
        public Boolean Delete(IEntity entity)
        {

            getISession.Delete(entity);
            getISession.Flush();
            return true;
        }
        /// <summary>一个NHibernate 查询字符串来一次性删除很多对象</summary>
        /// <returns></returns>
        public Int32 DeleteAll(string strWhere)
        {
            Int32 result = -1;

            result = (Int32)getISession.Delete(strWhere);
            getISession.Flush();

            return result;

        }
        public Int32 DeleteAll(AbstractExpressions strWhere)
        {
            Int32 result = -1;

            result = (Int32)getISession.Delete(strWhere.ToNhCriterion().ToString());
            getISession.Flush();

            return result;

        }

        /// <summary>检查这个对象是否已经存在Session中。如果对象不在，调用Save(object)来保存。如果对象存在，检查这个对象是否改变了。如果对象改变，调用Update(object)来更新。</summary>
        /// <returns></returns>
        public Boolean Save(IEntity entity)
        {
            getISession.SaveOrUpdate(entity);
            getISession.Flush();
            return true;
        }


        #region 事务
        public void BeginTrans()
        {
             getISession.BeginTransaction();
        }
        public void Commit()
        {
            if (getISession.Transaction == null) { throw new Exception("提交事务失败尚未初始化"); };
            getISession.Transaction.Commit();
        }
        public void Rollback()
        {
            if (getISession.Transaction == null) { throw new Exception("回滚失败事务尚未初始化"); };
            getISession.Transaction.Rollback();
        } 
        #endregion

        public TEntity Find(Field field, object obj)
        {
            TEntity t = default(TEntity);
          
            ICriteria crit = getISession.CreateCriteria<TEntity>();
            crit.Add(field.Eq(obj).ToNhCriterion());
            t = crit.UniqueResult<TEntity>();
            return t;
        }
        public TEntity Find(IList<Field> field, object[] obj)
        {
            TEntity t = default(TEntity);
            ICriteria crit = getISession.CreateCriteria<TEntity>();
            for (int i = 0; i < field.Count; i++)
            {
                crit.Add(field[i].Eq(obj[i]).ToNhCriterion());
            }
            t = crit.UniqueResult<TEntity>();
            return t;
        }
        public TEntity Find(AbstractExpressions icriterion)
        {
            TEntity t = default(TEntity);
            ICriteria crit = getISession.CreateCriteria<TEntity>();
            crit.Add(icriterion.ToNhCriterion());
            t = crit.UniqueResult<TEntity>();
            return t;
        }
        public TEntity FindByID(object id)
        {
           return getISession.Get<TEntity>(id);
        }

        public IEntityList<TEntity> FindAll(Field field, object obj)
        {
            EntityList<TEntity> list = null;
            ICriteria crit = getISession.CreateCriteria<TEntity>();
            crit.Add(field.Eq(obj).ToNhCriterion());
            list = new EntityList<TEntity>(crit.List<TEntity>());
            return list;
        }

        public IEntityList<TEntity> FindAll(IList<Field> field, object[] obj)
        {
            EntityList<TEntity> list = null;
            ICriteria crit = getISession.CreateCriteria<TEntity>();
            for (int i = 0; i < field.Count; i++)
            {
                crit.Add(field[i].Eq(obj[i]).ToNhCriterion());
            }
            list = new EntityList<TEntity>(crit.List<TEntity>());
            return list;
        }
        /// <summary>
        /// 查询当前表中的所有数据（如果业务数据量可能过大请慎用）
        /// </summary>
        /// <returns></returns>
        public IEntityList<TEntity> FindAll()
        {
            EntityList<TEntity> list = null;

            ICriteria crit = getISession.CreateCriteria<TEntity>();
            list = new EntityList<TEntity>(crit.List<TEntity>());

            return list;
        }

        /// <summary>
        ///根据条件查询数据表中的数据
        /// </summary>
        /// <returns></returns>
        public IEntityList<TEntity> FindAll(String strWhere, String strOrder)
        {
            EntityList<TEntity> list = null;

            List<TEntity> listtemp = getISession.CreateQuery(string.Format("from {0} where 1=1 {1} {2}", tableItem.Name, strWhere, strOrder)).List<TEntity>() as EntityList<TEntity>;
            list = new EntityList<TEntity>(listtemp);

            return list;
        }
        public IEntityList<TEntity> FindAll(AbstractExpressions icriterion, PxOrder order, Int32 startRowIndex, Int32 maximumRows)
        {
            EntityList<TEntity> list = null;
            ICriteria crit = getISession.CreateCriteria<TEntity>();
            if (icriterion != null) { crit.Add(icriterion.ToNhCriterion()); }
            if (order != null) { crit.AddOrder(order.ToOrderNh()); }
            crit.SetFirstResult(startRowIndex);
            crit.SetMaxResults(maximumRows);
            list = new EntityList<TEntity>(crit.List<TEntity>());
            return list;

        }
        public IEntityList<TEntity> FindAll(String strWhere, String orderClause, Int32 startRowIndex, Int32 maximumRows)
        {
            EntityList<TEntity> list = null;

            string hql = string.Format("from {0} where 1=1 {1} {2}", tableItem.Name, strWhere, orderClause);
            IQuery query = getISession.CreateQuery(hql);
            query.SetFirstResult(startRowIndex);
            query.SetMaxResults(maximumRows);
            list = new EntityList<TEntity>(query.List<TEntity>());
            return list;
        }
        public IEntityList<TEntity> FindAll(String Hql, Int32 startRowIndex, Int32 maximumRows)
        {
            EntityList<TEntity> list = null;

            IQuery query = getISession.CreateQuery(Hql);
            query.SetFirstResult(startRowIndex);
            query.SetMaxResults(maximumRows);
            list = new EntityList<TEntity>(query.List<TEntity>());
            return list;
        }


        public DataTable QueryHql(String Hql, Int32 startRowIndex, Int32 maximumRows)
        {
            DataTable list = null;
            IQuery query = getISession.CreateQuery(Hql);

            query.SetFirstResult(startRowIndex);
            query.SetMaxResults(maximumRows);
            IList<TEntity> listtemp = query.List<TEntity>();
            list = listtemp.IListToDataTable();
            return list;
        }
        //public IList QuerySql(String sql, Int32 startRowIndex, Int32 maximumRows)
        //{
        //    IList list = null;
        //    using (getISession)
        //    {
        //        IQuery query = getISession.CreateSQLQuery(sql);
        //        query.SetFirstResult(startRowIndex);
        //        query.SetMaxResults(maximumRows);
        //        list = query.List();

        //    }
        //    return list;
        //}
        public DataTable QuerySql(String sql)
        {
            DataTable list = null;
            IDbCommand cmd = getISession.Connection.CreateCommand();
            cmd.CommandText = sql;
            IDataReader reader = cmd.ExecuteReader();
            list = new DataTable();
            list.Load(reader);
            if (!reader.IsClosed) { reader.Close(); }
            return list;
        }
        public object QuerySqlScalar(String sql)
        {
            object obj = null;

            IDbCommand cmd = getISession.Connection.CreateCommand();
            cmd.CommandText = sql;
            obj = cmd.ExecuteScalar();
            return obj;
        }
        public object ExecuteScalar(String Hql)
        {
            object result = -1;
            IQuery query = getISession.CreateQuery(Hql);

            result = query.UniqueResult();
            return result;
        }
        public Int32 FindCount(String strWhere)
        {
            Int32 result = -1;
            string hql = string.Format("from {0} where 1=1 {1}", "count(1)", tableItem.Name, strWhere);
            IQuery query = getISession.CreateQuery(hql);

            result = Int32.Parse(query.UniqueResult().ToString());
            return result;
        }
        public Int32 FindCount(AbstractExpressions icriterion)
        {
            Int32 result = -1;
            ICriteria crit = getISession.CreateCriteria<TEntity>();
            if (icriterion != null) { crit.Add(icriterion.ToNhCriterion()); }
            crit.SetProjection(Projections.RowCount());
            result = crit.List().Count;
            return result;
        }
        public Int32 FindCount(String tableName, String whereClause)
        {
            Int32 result = -1;
            string hql = string.Format("select count(1) from {0} where 1=1 {1}", "count(1)", tableName, whereClause);
            IQuery query = getISession.CreateSQLQuery(hql);

            result = Int32.Parse(query.UniqueResult().ToString());
            return result;
        }
        /// <summary>最标准的查询数据。没有数据时返回空集合而不是null</summary>
        /// <remarks>
        /// 最经典的批量查询，看这个Select @selects From Table Where @whereClause Order By @orderClause Limit @startRowIndex,@maximumRows，你就明白各参数的意思了。
        /// </remarks>
        /// <param name="whereClause">条件字句，不带Where</param>
        /// <param name="orderClause">排序字句，不带Order By</param>
        /// <param name="selects">查询列，默认null表示所有字段</param>
        /// <param name="startRowIndex">开始行，0表示第一行</param>
        /// <param name="maximumRows">最大返回行数，0表示所有行</param>
        /// <returns>实体集</returns>
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public DataTable FindAllToTable(String whereClause, String orderClause, String selects, Int32 startRowIndex, Int32 maximumRows)
        {
            var session = this;

            #region 海量数据查询优化
            // 海量数据尾页查询优化
            // 在海量数据分页中，取越是后面页的数据越慢，可以考虑倒序的方式
            // 只有在百万数据，且开始行大于五十万时才使用

            // 如下优化，避免了每次都调用Meta.Count而导致形成一次查询，虽然这次查询时间损耗不大
            // 但是绝大多数查询，都不需要进行类似的海量数据优化，显然，这个startRowIndex将会挡住99%以上的浪费
            //Int64 count = 0;
            //if (startRowIndex > 500000 && (count = session.LongCount) > 1000000)
            //{
            //    // 计算本次查询的结果行数
            //    if (!String.IsNullOrEmpty(whereClause)) count = FindCount(whereClause, orderClause, selects, startRowIndex, maximumRows);
            //    // 游标在中间偏后
            //    if (startRowIndex * 2 > count)
            //    {
            //        String order = orderClause;
            //        Boolean bk = false; // 是否跳过

            //        #region 排序倒序
            //        // 默认是自增字段的降序
            //        FieldItem fi = Meta.Unique;
            //        if (String.IsNullOrEmpty(order) && fi != null && fi.IsIdentity) order = fi.Name + " Desc";

            //        if (!String.IsNullOrEmpty(order))
            //        {
            //            //2014-01-05 Modify by Apex
            //            //处理order by带有函数的情况，避免分隔时将函数拆分导致错误
            //            foreach (Match match in Regex.Matches(order, @"\([^\)]*\)", RegexOptions.Singleline))
            //            {
            //                order = order.Replace(match.Value, match.Value.Replace(",", "★"));
            //            }
            //            String[] ss = order.Split(',');
            //            var sb = new StringBuilder();
            //            foreach (String item in ss)
            //            {
            //                String fn = item;
            //                String od = "asc";

            //                Int32 p = fn.LastIndexOf(" ");
            //                if (p > 0)
            //                {
            //                    od = item.Substring(p).Trim().ToLower();
            //                    fn = item.Substring(0, p).Trim();
            //                }

            //                switch (od)
            //                {
            //                    case "asc":
            //                        od = "desc";
            //                        break;
            //                    case "desc":
            //                        //od = "asc";
            //                        od = null;
            //                        break;
            //                    default:
            //                        bk = true;
            //                        break;
            //                }
            //                if (bk) break;

            //                if (sb.Length > 0) sb.Append(", ");
            //                sb.AppendFormat("{0} {1}", fn, od);
            //            }

            //            order = sb.ToString().Replace("★", ",");
            //        }
            //        #endregion

            //        // 没有排序的实在不适合这种办法，因为没办法倒序
            //        if (!String.IsNullOrEmpty(order))
            //        {
            //            // 最大可用行数改为实际最大可用行数
            //            var max = (Int32)Math.Min(maximumRows, count - startRowIndex);
            //            //if (max <= 0) return null;
            //            if (max <= 0) return new EntityList<TEntity>();
            //            var start = (Int32)(count - (startRowIndex + maximumRows));

            //            var builder2 = CreateBuilder(whereClause, order, selects, start, max);
            //            var list = LoadData(session.Query(builder2, start, max));
            //            if (list == null || list.Count < 1) return list;
            //            // 因为这样取得的数据是倒过来的，所以这里需要再倒一次
            //            list.Reverse();
            //            return list;
            //        }
            //    }
            //}
            #endregion

            var builder = CreateBuilder(String.Empty, whereClause, orderClause, selects, startRowIndex, maximumRows);
            return Query(builder, startRowIndex, maximumRows);
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public DataTable FindAllToTable(String tableName, String whereClause, String orderClause, String selects, Int32 startRowIndex, Int32 maximumRows)
        {
            var session = this;

            #region 海量数据查询优化
            // 海量数据尾页查询优化
            // 在海量数据分页中，取越是后面页的数据越慢，可以考虑倒序的方式
            // 只有在百万数据，且开始行大于五十万时才使用

            // 如下优化，避免了每次都调用Meta.Count而导致形成一次查询，虽然这次查询时间损耗不大
            // 但是绝大多数查询，都不需要进行类似的海量数据优化，显然，这个startRowIndex将会挡住99%以上的浪费
            //Int64 count = 0;
            //if (startRowIndex > 500000 && (count = session.LongCount) > 1000000)
            //{
            //    // 计算本次查询的结果行数
            //    if (!String.IsNullOrEmpty(whereClause)) count = FindCount(whereClause, orderClause, selects, startRowIndex, maximumRows);
            //    // 游标在中间偏后
            //    if (startRowIndex * 2 > count)
            //    {
            //        String order = orderClause;
            //        Boolean bk = false; // 是否跳过

            //        #region 排序倒序
            //        // 默认是自增字段的降序
            //        FieldItem fi = Meta.Unique;
            //        if (String.IsNullOrEmpty(order) && fi != null && fi.IsIdentity) order = fi.Name + " Desc";

            //        if (!String.IsNullOrEmpty(order))
            //        {
            //            //2014-01-05 Modify by Apex
            //            //处理order by带有函数的情况，避免分隔时将函数拆分导致错误
            //            foreach (Match match in Regex.Matches(order, @"\([^\)]*\)", RegexOptions.Singleline))
            //            {
            //                order = order.Replace(match.Value, match.Value.Replace(",", "★"));
            //            }
            //            String[] ss = order.Split(',');
            //            var sb = new StringBuilder();
            //            foreach (String item in ss)
            //            {
            //                String fn = item;
            //                String od = "asc";

            //                Int32 p = fn.LastIndexOf(" ");
            //                if (p > 0)
            //                {
            //                    od = item.Substring(p).Trim().ToLower();
            //                    fn = item.Substring(0, p).Trim();
            //                }

            //                switch (od)
            //                {
            //                    case "asc":
            //                        od = "desc";
            //                        break;
            //                    case "desc":
            //                        //od = "asc";
            //                        od = null;
            //                        break;
            //                    default:
            //                        bk = true;
            //                        break;
            //                }
            //                if (bk) break;

            //                if (sb.Length > 0) sb.Append(", ");
            //                sb.AppendFormat("{0} {1}", fn, od);
            //            }

            //            order = sb.ToString().Replace("★", ",");
            //        }
            //        #endregion

            //        // 没有排序的实在不适合这种办法，因为没办法倒序
            //        if (!String.IsNullOrEmpty(order))
            //        {
            //            // 最大可用行数改为实际最大可用行数
            //            var max = (Int32)Math.Min(maximumRows, count - startRowIndex);
            //            //if (max <= 0) return null;
            //            if (max <= 0) return new EntityList<TEntity>();
            //            var start = (Int32)(count - (startRowIndex + maximumRows));

            //            var builder2 = CreateBuilder(whereClause, order, selects, start, max);
            //            var list = LoadData(session.Query(builder2, start, max));
            //            if (list == null || list.Count < 1) return list;
            //            // 因为这样取得的数据是倒过来的，所以这里需要再倒一次
            //            list.Reverse();
            //            return list;
            //        }
            //    }
            //}
            #endregion

            var builder = CreateBuilder(tableName, whereClause, orderClause, selects, startRowIndex, maximumRows);
            return Query(builder, startRowIndex, maximumRows);
        }
        /// <summary>执行SQL查询，返回记录集</summary>
        /// <param name="builder">SQL语句</param>
        /// <param name="startRowIndex">开始行，0表示第一行</param>
        /// <param name="maximumRows">最大返回行数，0表示所有行</param>
        /// <param name="tableNames">所依赖的表的表名</param>
        /// <returns></returns>
        private DataTable Query(SelectBuilder builder, Int32 startRowIndex, Int32 maximumRows)
        {
            builder = PageSplit(builder, startRowIndex, maximumRows);
            if (builder == null) return null;

            return QuerySql(builder.ToString());
        }
        private SelectBuilder CreateBuilder(String tableName, String whereClause, String orderClause, String selects, Int32 startRowIndex, Int32 maximumRows, Boolean needOrderByID = true)
        {
            var builder = new SelectBuilder();
            builder.Column = selects;
            if (!String.IsNullOrEmpty(tableName))
            {
                builder.Table = tableName;
            }
            else
            {
                builder.Table = TableItem.Name;
            }

            builder.OrderBy = orderClause;
            // 谨记：某些项目中可能在where中使用了GroupBy，在分页时可能报错
            builder.Where = whereClause;

            // XCode对于默认排序的规则：自增主键降序，其它情况默认
            // 返回所有记录
            if (!needOrderByID && startRowIndex <= 0 && maximumRows <= 0) return builder;

            Field fi = TableItem.Unique as Field;
            if (fi.Equals(null))
            {
                builder.Key = fi.Name;

                // 默认获取数据时，还是需要指定按照自增字段降序，符合使用习惯
                // 有GroupBy也不能加排序
                if (String.IsNullOrEmpty(builder.OrderBy) &&
                    String.IsNullOrEmpty(builder.GroupBy) &&
                    // 未指定查询字段的时候才默认加上排序，因为指定查询字段的很多时候是统计
                    (selects.IsNullOrWhiteSpace() || selects == "*")
                    )
                {
                    // 数字降序，其它升序
                   
                    var b = fi.Type.IsIntType();
                    builder.IsDesc = b;
                    // 修正没有设置builder.IsInt导致分页没有选择最佳的MaxMin的BUG，感谢 @RICH(20371423)
                    builder.IsInt = b;

                    builder.OrderBy = builder.KeyOrder;
                }
            }
            else
            {
                // 如果找不到唯一键，并且排序又为空，则采用全部字段一起，确保能够分页
                //if (String.IsNullOrEmpty(builder.OrderBy)) builder.Keys = Meta.FieldNames.ToArray();
            }
            return builder;
        }
        /// <summary>根据条件把普通查询SQL格式化为分页SQL。</summary>
        /// <remarks>
        /// 因为需要继承重写的原因，在数据类中并不方便缓存分页SQL。
        /// 所以在这里做缓存。
        /// </remarks>
        /// <param name="builder">查询生成器</param>
        /// <param name="startRowIndex">开始行，0表示第一行</param>
        /// <param name="maximumRows">最大返回行数，0表示所有行</param>
        /// <returns>分页SQL</returns>
        private SelectBuilder PageSplit(SelectBuilder builder, Int32 startRowIndex, Int32 maximumRows)
        {
            //builder.Table = FormatedTableName;
            return MSPageSplit.PageSplit(builder, startRowIndex, maximumRows, true);
        }


    }
}
