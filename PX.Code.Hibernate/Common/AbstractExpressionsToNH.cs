﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using PX.Core;

namespace PX.Code.Hibernate.Common
{
    public static class AbstractExpressionsToNH
    {
        public static AbstractCriterion ToNhCriterion(this AbstractExpressions cri)
        {
            AbstractCriterion acri=null;
            AbstractCriterion acritemp = null;
            for (int i = 0; i < cri.ExpressionList.Count; i++)
            {
                IExpression w = cri.ExpressionList[i];
                switch (w.ComparisonWhere)
                {
                    case ComparisonWhere.BetweenAnd:
                        acritemp = Restrictions.Between(w.ColumnName, w.ParamStartValue,w.ParamEndValue);
                        break;
                    case ComparisonWhere.Equals:
                        acritemp=Restrictions.Eq(w.ColumnName, w.ParamStartValue);
                        break;
                    case ComparisonWhere.GreaterOrEquals:
                        acritemp = Restrictions.Ge(w.ColumnName, w.ParamStartValue);
                        break;
                    case ComparisonWhere.GreaterThan:
                        acritemp = Restrictions.Gt(w.ColumnName, w.ParamStartValue);
                        break;
                    case ComparisonWhere.IsNull:
                        acritemp = Restrictions.IsNull(w.ColumnName);
                        break;
                    case ComparisonWhere.IsNotNull:
                        acritemp = Restrictions.IsNotNull(w.ColumnName);
                        break;
                    case ComparisonWhere.LessOrEquals:
                        acritemp = Restrictions.Le(w.ColumnName, w.ParamStartValue);
                        break;
                    case ComparisonWhere.LessThan:
                        acritemp = Restrictions.Lt(w.ColumnName, w.ParamStartValue);
                        break;
                    case ComparisonWhere.LikeAnyWhere:
                        acritemp = Restrictions.Like(w.ColumnName, w.ParamStartValue.ToString(),MatchMode.Anywhere);
                        break;
                    case ComparisonWhere.NotEquals:
                         acritemp =Restrictions.Not(Restrictions.Eq(w.ColumnName, w.ParamStartValue));
                        break;
                    default:
                        throw new Exception("枚举项没有穷举");

                }
                if (i==0)
                {
                    acri = acritemp;
                }
                else
                {
                    switch (cri.OperatorExpressionList[i-1].OperatorWhere)
                    {
                        case OperatorWhere.And:
                            acri = acri & acritemp;
                            break;
                        case OperatorWhere.Or:
                            acri = acri | acritemp;
                            break;
                        default:
                            break;
                    }
                }

            }
            return acri;
        }
    }
}
