﻿using Domain.IDomain;
using Domain.IDomain.IDomian;
using PX.Code.Base;
using PX.Code.Base.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain
{
    [Serializable]
    [EntityAttribute(ConnName="Con",Name="")]
    public partial class BS_EMPLOYEE: Entity<BS_EMPLOYEE>,IBS_EMPLOYEE
    {
        private Int32 _ID;
        /// <summary></summary>
        public virtual Int32  ID
        { 
            get { return _ID; }
            set { _ID = value; }
        }
        private String _EMP_NAME;
        /// <summary></summary>
        public virtual String  EMP_NAME
        { 
            get { return _EMP_NAME; }
            set { _EMP_NAME = value; }
        }
        private Decimal _ENTERPRISE_ID;
        /// <summary></summary>
        public virtual Decimal  ENTERPRISE_ID
        { 
            get { return _ENTERPRISE_ID; }
            set { _ENTERPRISE_ID = value; }
        }
        private Decimal _DEPART_ID;
        /// <summary></summary>
        public virtual Decimal  DEPART_ID
        { 
            get { return _DEPART_ID; }
            set { _DEPART_ID = value; }
        }
        private DateTime _BIRTHDAY;
        /// <summary></summary>
        public virtual DateTime  BIRTHDAY
        { 
            get { return _BIRTHDAY; }
            set { _BIRTHDAY = value; }
        }
        private String _ID_CARD;
        /// <summary></summary>
        public virtual String  ID_CARD
        { 
            get { return _ID_CARD; }
            set { _ID_CARD = value; }
        }
        private DateTime _HIRE_DATE;
        /// <summary></summary>
        public virtual DateTime  HIRE_DATE
        { 
            get { return _HIRE_DATE; }
            set { _HIRE_DATE = value; }
        }
        private String _MOBILE;
        /// <summary></summary>
        public virtual String  MOBILE
        { 
            get { return _MOBILE; }
            set { _MOBILE = value; }
        }
        private String _PHONE;
        /// <summary></summary>
        public virtual String  PHONE
        { 
            get { return _PHONE; }
            set { _PHONE = value; }
        }
        private DateTime _LEAVE_DATE;
        /// <summary></summary>
        public virtual DateTime  LEAVE_DATE
        { 
            get { return _LEAVE_DATE; }
            set { _LEAVE_DATE = value; }
        }
        private Decimal _IS_ENABLE;
        /// <summary></summary>
        public virtual Decimal  IS_ENABLE
        { 
            get { return _IS_ENABLE; }
            set { _IS_ENABLE = value; }
        }
        private Decimal _SEX;
        /// <summary></summary>
        public virtual Decimal  SEX
        { 
            get { return _SEX; }
            set { _SEX = value; }
        }
        private String _ADDRESS;
        /// <summary></summary>
        public virtual String  ADDRESS
        { 
            get { return _ADDRESS; }
            set { _ADDRESS = value; }
        }
        private String _DUTY;
        /// <summary></summary>
        public virtual String  DUTY
        { 
            get { return _DUTY; }
            set { _DUTY = value; }
        }
        private String _USER_NAME;
        /// <summary></summary>
        public virtual String  USER_NAME
        { 
            get { return _USER_NAME; }
            set { _USER_NAME = value; }
        }
        private String _PSW;
        /// <summary></summary>
        public virtual String  PSW
        { 
            get { return _PSW; }
            set { _PSW = value; }
        }
        #region 获取/设置 字段值
        /// <summary>
        /// 获取/设置 字段值。
        /// 一个索引，基类使用反射实现。
        /// 派生实体类可重写该索引，以避免反射带来的性能损耗
        /// </summary>
        /// <param name="name">字段名</param>
        /// <returns></returns>
        public override  Object this[String name]
        {
            get
            {
                switch (name)
                {
                     case __.ID : return _ID;
                     case __.EMP_NAME : return _EMP_NAME;
                     case __.ENTERPRISE_ID : return _ENTERPRISE_ID;
                     case __.DEPART_ID : return _DEPART_ID;
                     case __.BIRTHDAY : return _BIRTHDAY;
                     case __.ID_CARD : return _ID_CARD;
                     case __.HIRE_DATE : return _HIRE_DATE;
                     case __.MOBILE : return _MOBILE;
                     case __.PHONE : return _PHONE;
                     case __.LEAVE_DATE : return _LEAVE_DATE;
                     case __.IS_ENABLE : return _IS_ENABLE;
                     case __.SEX : return _SEX;
                     case __.ADDRESS : return _ADDRESS;
                     case __.DUTY : return _DUTY;
                     case __.USER_NAME : return _USER_NAME;
                     case __.PSW : return _PSW;
                    default: return null;
                }
            }
            set
            {
                switch (name)
                {
                      case __.ID: _ID = Convert.ToInt32(value); break;
                      case __.EMP_NAME: _EMP_NAME = Convert.ToString(value); break;
                      case __.ENTERPRISE_ID: _ENTERPRISE_ID = Convert.ToDecimal(value); break;
                      case __.DEPART_ID: _DEPART_ID = Convert.ToDecimal(value); break;
                      case __.BIRTHDAY: _BIRTHDAY = Convert.ToDateTime(value); break;
                      case __.ID_CARD: _ID_CARD = Convert.ToString(value); break;
                      case __.HIRE_DATE: _HIRE_DATE = Convert.ToDateTime(value); break;
                      case __.MOBILE: _MOBILE = Convert.ToString(value); break;
                      case __.PHONE: _PHONE = Convert.ToString(value); break;
                      case __.LEAVE_DATE: _LEAVE_DATE = Convert.ToDateTime(value); break;
                      case __.IS_ENABLE: _IS_ENABLE = Convert.ToDecimal(value); break;
                      case __.SEX: _SEX = Convert.ToDecimal(value); break;
                      case __.ADDRESS: _ADDRESS = Convert.ToString(value); break;
                      case __.DUTY: _DUTY = Convert.ToString(value); break;
                      case __.USER_NAME: _USER_NAME = Convert.ToString(value); break;
                      case __.PSW: _PSW = Convert.ToString(value); break;
                    default: break;
                }
            }
           
        }
        #endregion
        #region 字段名
        private static _Fields _fields = new _Fields();
        public virtual IBS_EMPLOYEE_ _X
        {
            get { return _fields; }
        }
        public static IBS_EMPLOYEE_ _
        {
            get { return _fields; }
        }
        ///<summary>取得BS_EMPLOYEE字段信息的快捷方式</summary>
        private  partial class _Fields:IBS_EMPLOYEE_
        {
            public _Fields()
            {
                Console.WriteLine("1111111111111111111111");
            }
            private Field _ID=FindByName(__.ID);
            /// <summary></summary>
            public Field ID {get{return _ID;}}
            private Field _EMP_NAME=FindByName(__.EMP_NAME);
            /// <summary></summary>
            public Field EMP_NAME {get{return _EMP_NAME;}}
            private Field _ENTERPRISE_ID=FindByName(__.ENTERPRISE_ID);
            /// <summary></summary>
            public Field ENTERPRISE_ID {get{return _ENTERPRISE_ID;}}
            private Field _DEPART_ID=FindByName(__.DEPART_ID);
            /// <summary></summary>
            public Field DEPART_ID {get{return _DEPART_ID;}}
            private Field _BIRTHDAY=FindByName(__.BIRTHDAY);
            /// <summary></summary>
            public Field BIRTHDAY {get{return _BIRTHDAY;}}
            private Field _ID_CARD=FindByName(__.ID_CARD);
            /// <summary></summary>
            public Field ID_CARD {get{return _ID_CARD;}}
            private Field _HIRE_DATE=FindByName(__.HIRE_DATE);
            /// <summary></summary>
            public Field HIRE_DATE {get{return _HIRE_DATE;}}
            private Field _MOBILE=FindByName(__.MOBILE);
            /// <summary></summary>
            public Field MOBILE {get{return _MOBILE;}}
            private Field _PHONE=FindByName(__.PHONE);
            /// <summary></summary>
            public Field PHONE {get{return _PHONE;}}
            private Field _LEAVE_DATE=FindByName(__.LEAVE_DATE);
            /// <summary></summary>
            public Field LEAVE_DATE {get{return _LEAVE_DATE;}}
            private Field _IS_ENABLE=FindByName(__.IS_ENABLE);
            /// <summary></summary>
            public Field IS_ENABLE {get{return _IS_ENABLE;}}
            private Field _SEX=FindByName(__.SEX);
            /// <summary></summary>
            public Field SEX {get{return _SEX;}}
            private Field _ADDRESS=FindByName(__.ADDRESS);
            /// <summary></summary>
            public Field ADDRESS {get{return _ADDRESS;}}
            private Field _DUTY=FindByName(__.DUTY);
            /// <summary></summary>
            public Field DUTY {get{return _DUTY;}}
            private Field _USER_NAME=FindByName(__.USER_NAME);
            /// <summary></summary>
            public Field USER_NAME {get{return _USER_NAME;}}
            private Field _PSW=FindByName(__.PSW);
            /// <summary></summary>
            public Field PSW {get{return _PSW;}}
            static  Field FindByName(String name) { return FindFieldByName(name); }
        }
        /// <summary>取得BS_EMPLOYEE字段名称的快捷方式</summary>
        partial class __
        {
            /// <summary></summary>
            public const String ID="ID";
            /// <summary></summary>
            public const String EMP_NAME="EMP_NAME";
            /// <summary></summary>
            public const String ENTERPRISE_ID="ENTERPRISE_ID";
            /// <summary></summary>
            public const String DEPART_ID="DEPART_ID";
            /// <summary></summary>
            public const String BIRTHDAY="BIRTHDAY";
            /// <summary></summary>
            public const String ID_CARD="ID_CARD";
            /// <summary></summary>
            public const String HIRE_DATE="HIRE_DATE";
            /// <summary></summary>
            public const String MOBILE="MOBILE";
            /// <summary></summary>
            public const String PHONE="PHONE";
            /// <summary></summary>
            public const String LEAVE_DATE="LEAVE_DATE";
            /// <summary></summary>
            public const String IS_ENABLE="IS_ENABLE";
            /// <summary></summary>
            public const String SEX="SEX";
            /// <summary></summary>
            public const String ADDRESS="ADDRESS";
            /// <summary></summary>
            public const String DUTY="DUTY";
            /// <summary></summary>
            public const String USER_NAME="USER_NAME";
            /// <summary></summary>
            public const String PSW="PSW";

        }
        #endregion
    }

 
 }
