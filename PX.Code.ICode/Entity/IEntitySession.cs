﻿using PX.Code.Base.Entity;
using PX.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PX.Code.Base.Entity
{
    /// <summary>实体会话接口</summary>
    public interface IEntitySession<TEntity> where TEntity :IEntity
    {
        void BeginTrans();
        void Commit();
        void Rollback();

        Int32 DeleteAll(AbstractExpressions strWhere);
        Int32 DeleteAll(String strWhere);

        Object ExecuteScalar(String Hql);
        TEntity Find(AbstractExpressions icriterion);
        TEntity Find(Field field, Object obj);
        TEntity Find(IList<Field> field, Object[] obj);
        IEntityList<TEntity> FindAll();
        IEntityList<TEntity> FindAll(Field field, Object obj);
        IEntityList<TEntity> FindAll(AbstractExpressions icriterion, PxOrder order, Int32 startRowIndex, Int32 maximumRows);
        IEntityList<TEntity> FindAll(IList<Field> field, Object[] obj);
        IEntityList<TEntity> FindAll(String Hql, Int32 startRowIndex, Int32 maximumRows);
        IEntityList<TEntity> FindAll(String strWhere, String orderClause, Int32 startRowIndex, Int32 maximumRows);
        IEntityList<TEntity> FindAll(String strWhere, String strOrder);
        DataTable FindAllToTable(String tableName, String whereClause, String orderClause, String selects, Int32 startRowIndex, Int32 maximumRows);
        DataTable FindAllToTable(String whereClause, String orderClause, String selects, Int32 startRowIndex, Int32 maximumRows);
        TEntity FindByID(Object id);
        Int32 FindCount(AbstractExpressions icriterion);
        Int32 FindCount(String strWhere);
        Int32 FindCount(String tableName, String whereClause);

        DataTable QueryHql(String Hql, Int32 startRowIndex, Int32 maximumRows);
        DataTable QuerySql(String sql);
        Object QuerySqlScalar(String sql);

        Boolean Save(IEntity entity);

        ITableItem TableItem { get; }

        /// <summary>把该对象持久化到数据库，添加/更新实体缓存。</summary>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        Int32 Insert(IEntity entity);

        /// <summary>更新数据库，同时更新实体缓存</summary>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        Boolean Update(IEntity entity);

        /// <summary>从数据库中删除该对象，同时从实体缓存中删除</summary>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        Boolean Delete(IEntity entity);

    }
}
