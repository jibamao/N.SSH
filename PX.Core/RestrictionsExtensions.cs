using System;
using System.Collections;
using System.Linq.Expressions;

namespace PX.Criterion
{
	public static class RestrictionExtensions
	{
		/// <summary>
		/// Apply a "like" restriction in a QueryOver expression
		/// Note: throws an exception outside of a QueryOver expression
		/// </summary>
		public static bool IsLike(this string projection, string comparison)
		{
			throw new Exception("Not to be used directly - use inside QueryOver expression");
		}

		/// <summary>
		/// Apply a "like" restriction in a QueryOver expression
		/// Note: throws an exception outside of a QueryOver expression
		/// </summary>
		public static bool IsLike(this string projection, string comparison, MatchMode matchMode)
		{
			throw new Exception("Not to be used directly - use inside QueryOver expression");
		}

		/// <summary>
		/// Apply a "like" restriction in a QueryOver expression
		/// Note: throws an exception outside of a QueryOver expression
		/// </summary>
		public static bool IsLike(this string projection, string comparison, MatchMode matchMode, char? escapeChar)
		{
			throw new Exception("Not to be used directly - use inside QueryOver expression");
		}

		/// <summary>
		/// Apply a "like" restriction in a QueryOver expression
		/// Note: throws an exception outside of a QueryOver expression
		/// </summary>
		public static bool IsInsensitiveLike(this string projection, string comparison)
		{
			throw new Exception("Not to be used directly - use inside QueryOver expression");
		}

		/// <summary>
		/// Apply a "like" restriction in a QueryOver expression
		/// Note: throws an exception outside of a QueryOver expression
		/// </summary>
		public static bool IsInsensitiveLike(this string projection, string comparison, MatchMode matchMode)
		{
			throw new Exception("Not to be used directly - use inside QueryOver expression");
		}

		/// <summary>
		/// Apply an "in" constraint to the named property 
		/// Note: throws an exception outside of a QueryOver expression
		/// </summary>
		public static bool IsIn(this object projection, object[] values)
		{
			throw new Exception("Not to be used directly - use inside QueryOver expression");
		}

		/// <summary>
		/// Apply an "in" constraint to the named property 
		/// Note: throws an exception outside of a QueryOver expression
		/// </summary>
		public static bool IsIn(this object projection, ICollection values)
		{
			throw new Exception("Not to be used directly - use inside QueryOver expression");
		}

		/// <summary>
		/// Apply a "between" constraint to the named property
		/// Note: throws an exception outside of a QueryOver expression
		/// </summary>
		public static RestrictionBetweenBuilder IsBetween(this object projection, object lo)
		{
			throw new Exception("Not to be used directly - use inside QueryOver expression");
		}

		public class RestrictionBetweenBuilder
		{
			public bool And(object hi)
			{
				throw new Exception("Not to be used directly - use inside QueryOver expression");
			}
		}
	}
}
