namespace PX.Criterion
{
	using System;
	using System.Collections.Generic;


	/// <summary>
	/// Superclass for an <see cref="ICriterion"/> that represents a
	/// constraint between two properties (with SQL binary operators).
	/// </summary>
	[Serializable]
	public abstract class PropertyExpression : AbstractCriterion
	{
		private readonly string _lhsPropertyName;
		private readonly string _rhsPropertyName;
		private readonly IProjection _lhsProjection;
		private readonly IProjection _rhsProjection;

		/// <summary>
		/// Initializes a new instance of the <see cref="PropertyExpression"/> class.
		/// </summary>
		/// <param name="lhsProjection">The projection.</param>
		/// <param name="rhsPropertyName">Name of the RHS property.</param>
		protected PropertyExpression(IProjection lhsProjection, string rhsPropertyName)
		{
			this._lhsProjection = lhsProjection;
			_rhsPropertyName = rhsPropertyName;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PropertyExpression"/> class.
		/// </summary>
		/// <param name="lhsProjection">The LHS projection.</param>
		/// <param name="rhsProjection">The RHS projection.</param>
		protected PropertyExpression(IProjection lhsProjection, IProjection rhsProjection)
		{
			this._lhsProjection = lhsProjection;
			this._rhsProjection = rhsProjection;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PropertyExpression"/> class.
		/// </summary>
		/// <param name="lhsPropertyName">Name of the LHS property.</param>
		/// <param name="rhsPropertyName">Name of the RHS property.</param>
		protected PropertyExpression(string lhsPropertyName, string rhsPropertyName)
		{
			this._lhsPropertyName = lhsPropertyName;
			this._rhsPropertyName = rhsPropertyName;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PropertyExpression"/> class.
		/// </summary>
		/// <param name="lhsPropertyName">Name of the LHS property.</param>
		/// <param name="rhsProjection">The RHS projection.</param>
		protected PropertyExpression(string lhsPropertyName, IProjection rhsProjection)
		{
			this._lhsPropertyName = lhsPropertyName;
			this._rhsProjection = rhsProjection;
		}

		/// <summary>
		/// Get the Sql operator to use for the property expression.
		/// </summary>
		protected abstract string Op { get; }


		/// <summary></summary>
		public override string ToString()
		{
			return (_lhsProjection ?? (object)_lhsPropertyName) + Op + _rhsPropertyName;
		}
	}
}
