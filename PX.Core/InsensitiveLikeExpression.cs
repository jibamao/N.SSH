using System;
using System.Collections.Generic;
using System.Linq;



namespace PX.Criterion
{
	/// <summary>
	/// An <see cref="ICriterion"/> that represents an "like" constraint
	/// that is <b>not</b> case sensitive.
	/// </summary>
	//TODO:H2.0.3 renamed this to ILikeExpression
	[Serializable]
	public class InsensitiveLikeExpression : AbstractCriterion
	{
		private readonly string propertyName;
		private readonly object value;
		private readonly IProjection projection;

		/// <summary>
		/// Initializes a new instance of the <see cref="InsensitiveLikeExpression"/> class.
		/// </summary>
		/// <param name="projection">The projection.</param>
		/// <param name="value">The value.</param>
		/// <param name="matchMode">The match mode.</param>
		public InsensitiveLikeExpression(IProjection projection, string value, MatchMode matchMode)
		{
			this.projection = projection;
			this.value = matchMode.ToMatchString(value);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="InsensitiveLikeExpression"/> class.
		/// </summary>
		/// <param name="projection">The projection.</param>
		/// <param name="value">The value.</param>
		public InsensitiveLikeExpression(IProjection projection, object value)
		{
			this.projection = projection;
			this.value = value;
		}

		/// <summary>
		/// Initialize a new instance of the <see cref="InsensitiveLikeExpression" /> 
		/// class for a named Property and its value.
		/// </summary>
		/// <param name="propertyName">The name of the Property in the class.</param>
		/// <param name="value">The value for the Property.</param>
		public InsensitiveLikeExpression(string propertyName, object value)
		{
			this.propertyName = propertyName;
			this.value = value;
		}

		public InsensitiveLikeExpression(string propertyName, string value, MatchMode matchMode)
			: this(propertyName, matchMode.ToMatchString(value))
		{
		}

		public override string ToString()
		{
			return (projection ?? (object)propertyName) + " ilike " + value;
		}
	}
}