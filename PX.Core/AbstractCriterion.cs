﻿using System;
using System.Collections.Generic;


namespace PX.Criterion
{
	/// <summary>
	/// Base class for <see cref="ICriterion"/> implementations.
	/// </summary>
	[Serializable]
	public abstract class AbstractCriterion : ICriterion
	{
		public abstract override string ToString();


		#region Operator Overloading

		public static AbstractCriterion operator &(AbstractCriterion lhs, AbstractCriterion rhs)
		{
			return new AndExpression(lhs, rhs);
		}

		public static AbstractCriterion operator |(AbstractCriterion lhs, AbstractCriterion rhs)
		{
			return new OrExpression(lhs, rhs);
		}


		public static AbstractCriterion operator &(AbstractCriterion lhs, AbstractEmptinessExpression rhs)
		{
			return new AndExpression(lhs, rhs);
		}

		public static AbstractCriterion operator |(AbstractCriterion lhs, AbstractEmptinessExpression rhs)
		{
			return new OrExpression(lhs, rhs);
		}


		public static AbstractCriterion operator !(AbstractCriterion crit)
		{
			return new NotExpression(crit);
		}

		/// <summary>
		/// See here for details:
		/// http://steve.emxsoftware.com/NET/Overloading+the++and++operators
		/// </summary>
		public static bool operator false(AbstractCriterion criteria)
		{
			return false;
		}

		/// <summary>
		/// See here for details:
		/// http://steve.emxsoftware.com/NET/Overloading+the++and++operators
		/// </summary>
		public static bool operator true(AbstractCriterion criteria)
		{
			return false;
		}

		#endregion
	}
}
