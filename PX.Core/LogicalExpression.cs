using System;
using System.Collections.Generic;



namespace PX.Criterion
{
	/// <summary>
	/// An <see cref="ICriterion"/> that combines two <see cref="ICriterion"/>s 
	/// with a operator (either "<c>and</c>" or "<c>or</c>") between them.
	/// </summary>
	[Serializable]
	public abstract class LogicalExpression : AbstractCriterion
	{
		private ICriterion _lhs;
		private ICriterion _rhs;

		/// <summary>
		/// Initialize a new instance of the <see cref="LogicalExpression" /> class that
		/// combines two other <see cref="ICriterion"/>s.
		/// </summary>
		/// <param name="lhs">The <see cref="ICriterion"/> to use in the Left Hand Side.</param>
		/// <param name="rhs">The <see cref="ICriterion"/> to use in the Right Hand Side.</param>
		protected LogicalExpression(ICriterion lhs, ICriterion rhs)
		{
			_lhs = lhs;
			_rhs = rhs;
		}

		/// <summary>
		/// Gets the <see cref="ICriterion"/> that will be on the Left Hand Side of the Op.
		/// </summary>
		protected ICriterion LeftHandSide
		{
			get { return _lhs; }
		}

		/// <summary>
		/// Gets the <see cref="ICriterion" /> that will be on the Right Hand Side of the Op.
		/// </summary>
		protected ICriterion RightHandSide
		{
			get { return _rhs; }
		}


		/// <summary>
		/// Get the Sql operator to put between the two <see cref="Expression"/>s.
		/// </summary>
		protected abstract string Op { get; } //protected ???

		/// <summary>
		/// Gets a string representation of the LogicalExpression.  
		/// </summary>
		/// <returns>
		/// The String contains the LeftHandSide.ToString() and the RightHandSide.ToString()
		/// joined by the Op.
		/// </returns>
		/// <remarks>
		/// This is not a well formed Sql fragment.  It is useful for logging what Expressions
		/// are being combined.
		/// </remarks>
		public override string ToString()
		{
			return '(' + _lhs.ToString() + ' ' + Op + ' ' + _rhs.ToString() + ')';
		}
	}
}
