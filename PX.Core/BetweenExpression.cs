using System;
using System.Collections.Generic;
using System.Linq;




namespace PX.Criterion
{
	/// <summary>
	/// An <see cref="ICriterion"/> that represents a "between" constraint.
	/// </summary>
	[Serializable]
	public class BetweenExpression : AbstractCriterion
	{
		private readonly object _hi;
		private readonly object _lo;
		private readonly IProjection _projection;

		private readonly string _propertyName;

		/// <summary>
		/// Initializes a new instance of the <see cref="BetweenExpression"/> class.
		/// </summary>
		/// <param name="projection">The _projection.</param>
		/// <param name="lo">The _lo.</param>
		/// <param name="hi">The _hi.</param>
		public BetweenExpression(IProjection projection, object lo, object hi)
		{
			this._projection = projection;
			this._lo = lo;
			this._hi = hi;
		}

		/// <summary>
		/// Initialize a new instance of the <see cref="BetweenExpression" /> class for
		/// the named Property.
		/// </summary>
		/// <param name="propertyName">The name of the Property of the Class.</param>
		/// <param name="lo">The low value for the BetweenExpression.</param>
		/// <param name="hi">The high value for the BetweenExpression.</param>
		public BetweenExpression(string propertyName, object lo, object hi)
		{
			_propertyName = propertyName;
			_lo = lo;
			_hi = hi;
		}
		public override string ToString()
		{
			return _propertyName + " between " + _lo + " and " + _hi;
		}
	}
}
