﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Core
{
    public class OperatorExpression : PX.Core.IOperatorExpression
    {
        /// <summary>
        /// 运算符：And/Or
        /// </summary>
        public OperatorWhere OperatorWhere { get; set; }
    }
}
