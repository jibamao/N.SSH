﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PX.Core
{
    public abstract class AbstractExpressions:IExpressions
    {
        public abstract List<IExpression> ExpressionList { get; set; }
        public abstract List<IOperatorExpression> OperatorExpressionList { get; set; }
        #region Operator 

        public static AbstractExpressions operator &(AbstractExpressions lhs, AbstractExpressions rhs)
        {
            return new AndExpressions(lhs, rhs);
        }

        public static AbstractExpressions operator |(AbstractExpressions lhs, AbstractExpressions rhs)
        {
            return new OrExpressions(lhs, rhs);
        }
        #endregion
    }
}
