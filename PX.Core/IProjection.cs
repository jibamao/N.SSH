using System;
using System.Collections.Generic;



namespace PX.Criterion
{
	public interface IProjection
	{

		/// <summary>
		/// Get the user-visible aliases for this projection (ie. the ones that will be passed to the ResultTransformer)
		/// </summary>
		string[] Aliases { get; }

		/// <summary>
		/// Does this projection specify grouping attributes?
		/// </summary>
		bool IsGrouped { get; }

		/// <summary>
		/// Does this projection specify aggregate attributes?
		/// </summary>
		bool IsAggregate { get; }

	}
}
