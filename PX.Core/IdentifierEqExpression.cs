using System;
using System.Collections.Generic;
using System.Linq;


namespace PX.Criterion
{
	/// <summary>
	/// An identifier constraint
	/// </summary>
	[Serializable]
	public class IdentifierEqExpression : AbstractCriterion
	{
		private readonly object value;

		public IdentifierEqExpression(IProjection projection)
		{
			_projection = projection;
		}

		private readonly IProjection _projection;

		public IdentifierEqExpression(object value)
		{
			this.value = value;
		}
		public override string ToString()
		{
			return (_projection != null ? _projection.ToString() : "ID") + " == " + value;
		}
	}
}
